import { Component, OnInit } from '@angular/core';
import {ApiService} from  '../../service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/model/user';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public userList: User[];

  constructor( private apiService:ApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    this.apiService.getAllUsers().subscribe((resp)=>{
      this.userList =resp;
    });
  }

  onSubmit(){

    

   

  }

}
