import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import {ApiService} from  '../../service/api.service';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  error = '';
  errorMessage='';
  authenticationFlag=false;

  constructor(private formBuilder: FormBuilder,
    private apiService:ApiService,
    private route: ActivatedRoute,
    private router: Router,) { 
    this.loginForm = this.formBuilder.group({
      
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
      
    });
  }

  ngOnInit(): void {
  }

  onSubmit(){

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.apiService.login(this.loginForm.value).subscribe((res)=>{
      this.router.navigate(['/home']);
    },
    error => {
      this.error = error;
      this.authenticationFlag=true;
      this.errorMessage = `${error.error.errorMessage}`;
  });

  }

}
