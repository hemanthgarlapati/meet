import { Injectable } from '@angular/core';
import { HttpClient,HttpParams} from '@angular/common/http' ;
import {User} from '../model/user';
import {Login} from '../model/login';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public registerUser(user: User){
    return this.httpClient.post("http://localhost:8081/api/v1/users",user);
  }

  public login(login: Login){
    return this.httpClient.post("http://localhost:8081/api/v1/login",login);
  }

  public getAllUsers() : Observable<any>{
    let data = new HttpParams().set('userId', "1");
    return this.httpClient.get("http://localhost:8081/api/v1/users",{ params: data});
  }
}
