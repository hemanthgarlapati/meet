/**
 * 
 */
package com.example.meet.enums;

/**
 * @author hemanth.garlapati
 *
 * 
 */
public enum Gender {
	MALE("male"),FEMALE("female");
	
	String genderType;

	/**
	 * @param genderType
	 */
	private Gender(String genderType) {
		this.genderType = genderType;
	}

	/**
	 * @return the genderType
	 */
	public String getGenderType() {
		return genderType;
	}
	

}
