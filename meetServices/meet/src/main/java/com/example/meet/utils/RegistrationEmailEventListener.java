package com.example.meet.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class RegistrationEmailEventListener implements ApplicationListener<UserRegistrationEvent> {

	@Autowired
	private JavaMailSender emailSender;
	
	@Autowired
	private Environment environment;

	@Override
	public void onApplicationEvent(UserRegistrationEvent event) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("noreply@gmail.com");
		message.setTo(event.getUser().getEmail());
		message.setSubject("Meet App Email Verification");
		message.setText("Account activation link: "+environment.getProperty("server.url")+"/api/v1/mail?userId="+event.getUser().getUserId());
		emailSender.send(message);
	}
}