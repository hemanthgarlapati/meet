/**
 * 
 */
package com.example.meet.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Component
public class StringToLocalDateConvertor {

	public LocalDate convertStringToLocalDate(String date) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(date, formatter);

	}

}
