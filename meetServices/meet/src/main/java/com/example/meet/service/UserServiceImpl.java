/**
 * 
 */
package com.example.meet.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.meet.config.ConfigurationManager;
import com.example.meet.dto.ResponseDto;
import com.example.meet.dto.UserDto;
import com.example.meet.encryption.Encryption;
import com.example.meet.entities.User;
import com.example.meet.enums.Gender;
import com.example.meet.exception.UserNotFoundException;
import com.example.meet.repository.UserRepository;
import com.example.meet.utils.StringToLocalDateConvertor;
import com.example.meet.utils.UserPresence;
import com.example.meet.utils.UserRegistrationEvent;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	ConfigurationManager configurationManager;

	@Autowired
	Encryption encryption;

	@Autowired
	StringToLocalDateConvertor localDateConvertor;

	@Autowired
	UserPresence userPresence;

	@Autowired
	ApplicationEventPublisher applicationEventPublisher;

	/**
	 * This method is for user registration
	 * 
	 * @param userDto
	 * @return ResponseDto
	 */
	public ResponseDto registerUser(UserDto userDto) {

		LocalDate dateOfBirth = localDateConvertor.convertStringToLocalDate(userDto.getDateOfBirth());

		Optional<User> optionalUser = userRepository.findByEmail(userDto.getEmail());

		User user = new User();

		BeanUtils.copyProperties(userDto, user);

		if (optionalUser.isPresent()) {

			user.setUserId(optionalUser.get().getUserId());
		}

		user.setDateOfBirth(dateOfBirth);
		user.setPassword(encryption.encrypt(userDto.getPassword()));
		user.setIsActive(false);
		userRepository.save(user);

		applicationEventPublisher.publishEvent(new UserRegistrationEvent(user));

		return new ResponseDto(HttpStatus.CREATED.value(), configurationManager.getRegistrationSuccessMsg());
	}

	/**
	 * This method is for fetching the users belonging to other gender
	 * 
	 * @param userId
	 * @return List<User>
	 */
	public List<User> findByGender(Long userId) {

		Optional<User> optionalUser = userPresence.checkForValidUser(userId);

		if (optionalUser.get().getGender().equals(Gender.MALE)) {
			return userRepository.findByGenderAndIsActive(Gender.FEMALE, true);
		}

		return userRepository.findByGenderAndIsActive(Gender.MALE, true);
	}

	/**
	 * This method is for fetching the users belonging to other gender
	 * 
	 * @param userId
	 * @return ResponseDto
	 */
	public ResponseDto deleteUser(Long userId) {

		Optional<User> optionalUser = userPresence.checkForValidUser(userId);

		optionalUser.get().setIsActive(false);
		userRepository.save(optionalUser.get());

		ResponseDto responseDto = new ResponseDto(HttpStatus.NO_CONTENT.value(),
				configurationManager.getUserDeletedMsg());

		return responseDto;
	}

	/**
	 * This method is for fetching the user details based on id
	 * 
	 * @param userId
	 * @return UserDto
	 */
	public UserDto getUserById(Long userId) {

		Optional<User> optionalUser = userPresence.checkForValidUser(userId);

		UserDto userDto = new UserDto();

		BeanUtils.copyProperties(optionalUser.get(), userDto);
		userDto.setDateOfBirth(optionalUser.get().getDateOfBirth().toString());

		return userDto;

	}

	/**
	 * This method is for verifying the user
	 * 
	 * @param userId
	 * @return ResponseDto
	 */
	public ResponseDto verifyUser(Long userId) {

		Optional<User> optionalUser = userRepository.findById(userId);
		
		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException(configurationManager.getUserNotFoundMsg());
		}

		optionalUser.get().setIsActive(true);

		userRepository.save(optionalUser.get());

		return new ResponseDto(HttpStatus.CREATED.value(), configurationManager.getUserActivatedMsg());

	}
}
