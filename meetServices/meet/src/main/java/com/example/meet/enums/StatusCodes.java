package com.example.meet.enums;
/**
 * @author hemanth.garlapati
 *
 */
public enum StatusCodes {
	VALIDATION_FAILED(4001l),INTERNAL_SERVER_ERROR(500l),SUCCESS(200L),USER_NOT_FOUND(4041l);

	Long StatusCode;

	StatusCodes(Long value) {
		this.StatusCode = value;
	}

	public Long getStatusCode() {
		return StatusCode;
	}

}