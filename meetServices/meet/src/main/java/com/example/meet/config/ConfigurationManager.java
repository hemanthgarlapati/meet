/**
 * 
 */
package com.example.meet.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

/**
 * @author hemanth.garlapati
 *
 */
@Configuration
@ConfigurationProperties
@PropertySource("classpath:messages-en.properties")
@Data
public class ConfigurationManager {

	String registrationSuccessMsg;
	String loginSuccessMsg;
	String loginFailedMsg;
	String userNotFoundMsg;
	String userDeletedMsg;
	String userActivatedMsg;
	

	
}
