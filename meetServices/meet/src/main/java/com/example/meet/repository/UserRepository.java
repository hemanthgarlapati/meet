/**
 * 
 */
package com.example.meet.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.meet.entities.User;
import com.example.meet.enums.Gender;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long>{
	
	Optional<User> findByEmail(String email);
	Optional<User> findByUserIdAndIsActive(Long userId,boolean isActive);
	Optional<User> findByEmailAndPasswordAndIsActive(String email, String password,boolean isActive);
	List<User> findByGenderAndIsActive(Gender gender,boolean isActive);
}
