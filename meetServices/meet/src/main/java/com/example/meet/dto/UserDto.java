/**
 * 
 */
package com.example.meet.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.stereotype.Component;

import com.example.meet.constant.Constants;
import com.example.meet.enums.Gender;
import com.example.meet.validator.ValidPassword;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class UserDto {

	@NotBlank(message = Constants.NAME_NOT_EMPTY)
	private String name;

	@NotBlank(message = Constants.MAIL_NOT_EMPTY)
	@Email(message = Constants.EMAIL_FORMAT)
	private String email;

	
	private Gender gender;

	@NotBlank(message = Constants.PASSWORD_NOT_EMPTY)
	@ValidPassword
	@JsonIgnore
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;

	@NotBlank(message = Constants.DOB_NOT_EMPTY)
	private String dateOfBirth;

	private String image;

}
