/**
 * 
 */
package com.example.meet.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.meet.dto.ResponseDto;
import com.example.meet.dto.UserDto;
import com.example.meet.entities.User;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Service
public interface UserService {
	
	public ResponseDto registerUser(UserDto userDto);
	public List<User> findByGender(Long userId);
	public ResponseDto deleteUser(Long userId);
	public UserDto getUserById(Long userId);
	public ResponseDto verifyUser(Long userId);

}
