/**
 * 
 */
package com.example.meet.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.example.meet.constant.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequestDto {
	
	@Email(message = Constants.EMAIL_FORMAT)
	@NotBlank(message = Constants.MAIL_NOT_EMPTY)
	private String email;
	private String password;

}
