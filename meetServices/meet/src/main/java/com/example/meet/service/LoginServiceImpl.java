/**
 * 
 */
package com.example.meet.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.meet.config.ConfigurationManager;
import com.example.meet.dto.LoginRequestDto;
import com.example.meet.dto.LoginResponseDto;
import com.example.meet.encryption.Encryption;
import com.example.meet.entities.User;
import com.example.meet.enums.StatusCodes;
import com.example.meet.exception.UserNotFoundException;
import com.example.meet.repository.UserRepository;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	Encryption encryption;
	
	@Autowired
	ConfigurationManager configurationManager;
	
	public LoginResponseDto login(LoginRequestDto loginRequestDto) {
		
		Optional<User> optionalUser=userRepository.findByEmailAndPasswordAndIsActive(loginRequestDto.getEmail(), encryption.encrypt(loginRequestDto.getPassword()),true);
		if(!optionalUser.isPresent()) {
			throw new UserNotFoundException(configurationManager.getLoginFailedMsg());
		}
		
		LoginResponseDto loginResponseDto=new LoginResponseDto();
		loginResponseDto.setCode(StatusCodes.SUCCESS.getStatusCode());
		loginResponseDto.setMessage(configurationManager.getLoginSuccessMsg());
		loginResponseDto.setUserId(optionalUser.get().getUserId());
		loginResponseDto.setUserName(optionalUser.get().getName());
		
		return loginResponseDto;
		
	}

}
