/**
 * 
 */
package com.example.meet.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponseDto {
	
	private Long code;
	private String message;
	private Long userId;
	private String userName;

}
