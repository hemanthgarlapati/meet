/**
 * 
 */
package com.example.meet.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.meet.constant.Constants;
import com.example.meet.dto.LoginRequestDto;
import com.example.meet.dto.LoginResponseDto;
import com.example.meet.service.LoginService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

	@Autowired
	LoginService loginService;
	
	

	/**
	 * This method is for user registration
	 * 
	 * @param loginDto
	 * @return ResponseEntity<ResponseDto>
	 */
	@ApiOperation(value = "User Login")
	@ApiResponses(value = { @ApiResponse(code = 4041, message = Constants.LOGIN_FAILED),
			@ApiResponse(code = 4002, message = Constants.MAIL_NOT_EMPTY),
			@ApiResponse(code = 4005, message = Constants.EMAIL_FORMAT) })
	@PostMapping("/login")
	public ResponseEntity<LoginResponseDto> login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
		
		return new ResponseEntity<>(loginService.login(loginRequestDto), HttpStatus.OK);
	}

}
