/**
 * 
 */
package com.example.meet.utils;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.meet.config.ConfigurationManager;
import com.example.meet.entities.User;
import com.example.meet.exception.UserNotFoundException;
import com.example.meet.repository.UserRepository;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Component
public class UserPresence {
	
	@Autowired
	UserRepository  userRepository;
	
	@Autowired
	ConfigurationManager configurationManager;
	
	public Optional<User> checkForValidUser(Long userId) {
		Optional<User> optionalUser = userRepository.findByUserIdAndIsActive(userId,true);

		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException(configurationManager.getUserNotFoundMsg());
		}
		
		return optionalUser;

	}

}
