/**
 * 
 */
package com.example.meet.constant;

/**
 * @author hemanth.garlapati
 *
 * 
 */
public class Constants {

	
	  private Constants() {
	  
	  }
	 
	public static final String NAME_NOT_EMPTY = "Name is mandatory";
	public static final String MAIL_NOT_EMPTY = "Email is mandatory";
	public static final String GENDER_NOT_EMPTY = "Gender is mandatory";
	public static final String PASSWORD_NOT_EMPTY = "Password is mandatory";
	public static final String DOB_NOT_EMPTY = "Date Of Birth is mandatory";
	public static final String EMAIL_FORMAT = "please provide a valid email";
	public static final String LOGIN_FAILED = "login failed, please provide valid credentials";
	public static final String USER_NOT_FOUND="user not found";

}
