package com.example.meet.utils;

import org.springframework.context.ApplicationEvent;

import com.example.meet.entities.User;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRegistrationEvent extends ApplicationEvent {
	private User user;

	private static final long serialVersionUID = 1L;

	public UserRegistrationEvent(User user) {
		super(user);
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}