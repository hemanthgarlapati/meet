/**
 * 
 */
package com.example.meet.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.meet.constant.Constants;
import com.example.meet.dto.ResponseDto;
import com.example.meet.dto.UserDto;
import com.example.meet.entities.User;
import com.example.meet.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

	@Autowired
	UserService userService;

	/**
	 * This method is for user registration
	 * 
	 * @param userDto
	 * @return ResponseEntity<ResponseDto>
	 */
	@ApiOperation(value = "User Registration")
	@ApiResponses(value = { @ApiResponse(code = 4001, message = Constants.NAME_NOT_EMPTY),
			@ApiResponse(code = 4002, message = Constants.MAIL_NOT_EMPTY),
			@ApiResponse(code = 4003, message = Constants.PASSWORD_NOT_EMPTY),
			@ApiResponse(code = 4004, message = Constants.DOB_NOT_EMPTY),
			@ApiResponse(code = 4005, message = Constants.EMAIL_FORMAT) })
	@PostMapping("/v1/users")
	public ResponseEntity<ResponseDto> registerUser(@Valid @RequestBody UserDto userDto) {
		return new ResponseEntity<>(userService.registerUser(userDto), HttpStatus.CREATED);
	}

	/**
	 * This method is for retrieving the users(either male or female)
	 * 
	 * @param nothing
	 * @return ResponseEntity<List<User>>
	 */
	@ApiOperation(value = "Fetch Users")
	@ApiResponses(value = { @ApiResponse(code = 4041, message = Constants.USER_NOT_FOUND) })
	@GetMapping("/v1/users")
	public ResponseEntity<List<User>> getUsers(@RequestParam Long userId) {
		return new ResponseEntity<>(userService.findByGender(userId), HttpStatus.OK);
	}

	/**
	 * This method is for retrieving the users(either male or female)
	 * 
	 * @param nothing
	 * @return ResponseEntity<List<User>>
	 */
	@ApiOperation(value = "Fetch User by Id")
	@ApiResponses(value = { @ApiResponse(code = 4041, message = Constants.USER_NOT_FOUND) })
	@GetMapping("/v1/users/{id}")
	public ResponseEntity<UserDto> getUserById(@PathVariable Long id) {
		return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
	}

	/**
	 * This method is for deleting the user
	 * 
	 * @param id
	 * @return ResponseEntity<ResponseDto>
	 */
	@ApiOperation(value = "Delete User")
	@ApiResponses(value = { @ApiResponse(code = 4041, message = Constants.USER_NOT_FOUND) })
	@DeleteMapping("/v1/users/{id}")
	public ResponseEntity<ResponseDto> deleteUser(@PathVariable Long id) {
		return new ResponseEntity<>(userService.deleteUser(id), HttpStatus.OK);
	}
	
	/**
	 * This method is for user registration
	 * 
	 * @param userId
	 * @return ResponseEntity<Object>
	 */
	@ApiOperation(value = "Email verification after register")
	@ApiResponses(value = { @ApiResponse(code = 4041, message = Constants.USER_NOT_FOUND) })
	@PostMapping("/v1/users/mail")
	public ResponseEntity<Object> VerifyUser(@RequestParam Long userId) {
		return new ResponseEntity<>(userService.verifyUser(userId), HttpStatus.OK);
	}

}
