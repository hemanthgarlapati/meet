/**
 * 
 */
package com.example.meet.service;

import org.springframework.stereotype.Service;

import com.example.meet.dto.LoginRequestDto;
import com.example.meet.dto.LoginResponseDto;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Service
public interface LoginService {
	
	public LoginResponseDto login(LoginRequestDto loginRequestDto);
}
